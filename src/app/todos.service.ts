import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})


export class TodosService {

  addTodo(text:String){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos')
      .push({'text':text});
    })

  }
  update(key,text){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text})
    })
  }
  delete(key){

    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos')
      .remove(key);
    })

  }

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }
}
