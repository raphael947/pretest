import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent } from './todos/todos.component';
// angular material
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
// firebase modules
import {AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodoComponent,
    RegistrationComponent,
    LoginComponent,
    CodesComponent,
    TodosComponent,
    UsertodosComponent
  ],
  imports: [
    MatCardModule,
    MatButtonModule ,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,

    RouterModule.forRoot([
      {path:'', component:TodosComponent},
      {path:'register', component:RegistrationComponent},
      {path:'login', component:LoginComponent},
      {path:'codes', component:CodesComponent},
      {path:'users', component: UsertodosComponent},

      {path:'**', component:TodosComponent}
    ])
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
