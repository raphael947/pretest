import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todoTextFromTodo = "Text not so far";
  showText($event){
    this.todoTextFromTodo = $event;
  }

  addTodo(){
    this.todosService.addTodo(this.text);
    this.text = '';
  }
  /*todos= [
    {"text": "Study json", "id":1},
    {"text": "Do final project", "id":2}];
    */
   todos= [];
   text:string;
  constructor(private db:AngularFireDatabase, private authService:AuthService, private todosService:TodosService) { }

  ngOnInit() {

    this.authService.user.subscribe(user=>{

      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          todos.forEach(
            todo =>{
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
            
          )
        }
      )

    })
  
    
  }

}
